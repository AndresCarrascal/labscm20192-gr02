package co.edu.udea.compumovil.gr02_20192.lab2activities.Interfaces

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.User

@Dao
interface UserDao {
    @Insert
    fun insertUser(user: User)

    @Query("SELECT * FROM users WHERE :user = usuario LIMIT 1")
    fun validateUser(user: String): User

    @Query("SELECT * FROM users WHERE activo = :valor LIMIT 1")
    fun autoLoguin(valor: Boolean): User

    @Query("UPDATE users SET activo = :valor")
    fun logOut(valor: Boolean)

    @Update
    fun updateUser(user: User)

    @Query("SELECT * FROM users")
    fun getUsers(): List<User>

    @Query("SELECT * FROM users WHERE usuario = :user LIMIT 1")
    fun getUserDuplicate(user: String): User
}