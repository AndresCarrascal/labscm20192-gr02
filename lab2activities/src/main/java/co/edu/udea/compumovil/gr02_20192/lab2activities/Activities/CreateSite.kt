package co.edu.udea.compumovil.gr02_20192.lab2activities.Activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import co.edu.udea.compumovil.gr02_20192.lab2activities.DataBase.AppDataBase
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.POI
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.User
import co.edu.udea.compumovil.gr02_20192.lab2activities.R
import kotlinx.android.synthetic.main.activity_create_site.*
import org.jetbrains.anko.doAsync
import java.io.ByteArrayOutputStream


class CreateSite : AppCompatActivity() {

    private val db = AppDataBase.getDataBase(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_site)
    }

    fun createSite(view: View) {
        var userSave: User?
        if (name.text.toString() != "" && description.text.toString() != "" && points.text.toString() != "") {
            doAsync {
                userSave = db.userDao().autoLoguin(true)
                val drawable = imageNewSite.getDrawable() as BitmapDrawable
                val bitmap = drawable.bitmap
                val stream1 = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream1)
                val bitMapData1 = stream1.toByteArray()
                val site1 = POI(
                    bitMapData1,
                    name.text.toString(),
                    description.text.toString(),
                    points.text.toString(),
                    userSave!!.codigo
                )
                db.poiDao().insertPoi(site1)
                runOnUiThread {
                    returnListSites()
                }
            }
        } else {
            Toast.makeText(this, R.string.missingDateToEnter, Toast.LENGTH_SHORT).show()
        }
    }

    fun add(view: View) {
        intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/"
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), 10)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            imageNewSite.setImageURI(data?.data)
        }
    }

    fun returnListSites() {
        val intent = Intent(this, DataInformation::class.java)
        startActivity(intent)
    }
}
