package co.edu.udea.compumovil.gr02_20192.lab2activities.Adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.POI
import co.edu.udea.compumovil.gr02_20192.lab2activities.R
import kotlinx.android.synthetic.main.items_sites.view.*

class SitesAdapter(private val mContext: Context, private val listSites: List<POI>) :
    ArrayAdapter<POI>(mContext, 0, listSites) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.items_sites, parent, false)
        val sites = listSites[position]
        val image = getImage(sites.image)

        layout.image.setImageBitmap(image)
        layout.name.text = sites.name
        layout.punctuation.text = sites.punctuation
        layout.description.text = sites.description
        return layout
    }

    fun getImage(image: ByteArray?): Bitmap? {
        if (image != null) {
            return BitmapFactory.decodeByteArray(image, 0, image.size)
        } else {
            return null;
        }
    }
}