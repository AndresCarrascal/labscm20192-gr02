package co.edu.udea.compumovil.gr02_20192.lab2activities.Activities

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.POI
import co.edu.udea.compumovil.gr02_20192.lab2activities.R
import kotlinx.android.synthetic.main.activity_detail_data_information.description
import kotlinx.android.synthetic.main.activity_detail_data_information.name
import kotlinx.android.synthetic.main.activity_detail_data_information.punctuation
import kotlinx.android.synthetic.main.activity_detail_data_information.image

class DetailDataInformation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_data_information)

        val POI = intent.getSerializableExtra("POI") as POI

        val imageNew = getImage(POI.image)

        image.setImageBitmap(imageNew)
        name.text = POI.name
        description.text = POI.description
        punctuation.text = POI.punctuation
    }

    fun getImage(image: ByteArray?): Bitmap? {
        if (image != null) {
            return BitmapFactory.decodeByteArray(image, 0, image.size)
        } else {
            return null
        }
    }
}
