package co.edu.udea.compumovil.gr02_20192.lab2activities.Activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import co.edu.udea.compumovil.gr02_20192.lab2activities.DataBase.AppDataBase
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.User
import co.edu.udea.compumovil.gr02_20192.lab2activities.R
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync

class Login : AppCompatActivity() {

    private val db = AppDataBase.getDataBase(this)
    private var data: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun Login(view: View) {
        doAsync {
            data = db.userDao().validateUser(name.text.toString())
            runOnUiThread {
                validateData(data, view)
            }
        }
    }

    fun validateData(data: User?, view: View) {
        if (data != null) {
            val user =
                User(data.correo, data.usuario, data.contraseña, true, data.codigo)
            doAsync {
                db.userDao().updateUser(user)
                runOnUiThread {
                    val intent = Intent(baseContext, DataInformation::class.java)
                    startActivity(intent)
                }
            }
        } else {
            Toast.makeText(this, R.string.Username_does_not_exist, Toast.LENGTH_SHORT).show()
        }
    }

    fun createAccount(view: View) {
        val intent = Intent(this, Register::class.java)
        startActivity(intent)
    }
}
