package co.edu.udea.compumovil.gr02_20192.lab2activities.Activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import co.edu.udea.compumovil.gr02_20192.lab2activities.DataBase.AppDataBase
import co.edu.udea.compumovil.gr02_20192.lab2activities.R
import org.jetbrains.anko.doAsync

class Splash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val db = AppDataBase.getDataBase(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val background = object : Thread() {
            override fun run() {
                try {
                    sleep(2000)
                    doAsync {
                        var data = db.userDao().autoLoguin(true)
                        runOnUiThread {
                            if (data != null) {
                                val intent = Intent(baseContext, DataInformation::class.java)
                                startActivity(intent)
                            } else {
                                val intent = Intent(baseContext, Login::class.java)
                                startActivity(intent)
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        background.start()
    }
}
