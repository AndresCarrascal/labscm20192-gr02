package co.edu.udea.compumovil.gr02_20192.lab2activities.Activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import co.edu.udea.compumovil.gr02_20192.lab2activities.Adapters.SitesAdapter
import co.edu.udea.compumovil.gr02_20192.lab2activities.DataBase.AppDataBase
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.POI
import co.edu.udea.compumovil.gr02_20192.lab2activities.R
import kotlinx.android.synthetic.main.activity_data_information.*
import org.jetbrains.anko.doAsync


class DataInformation : AppCompatActivity() {

    private val db = AppDataBase.getDataBase(this)
    private var lisPOI = emptyList<POI>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_information)
        createList()
    }

    fun createList() {
        doAsync {
            var data = db.userDao().autoLoguin(true)
            runOnUiThread {
                addList(data.codigo)
            }
        }
    }


    // convert from bitmap to byte array
    fun addList(code: Int) {
        db.poiDao().getAllForUser(code).observe(this, Observer {
            lisPOI = it
            val adapter = SitesAdapter(this, lisPOI)
            myList.adapter = adapter
            myList.setOnItemClickListener { parent, view, position, id ->
                val intent = Intent(baseContext, DetailDataInformation::class.java)
                intent.putExtra("POI", lisPOI[position])
                startActivity(intent)
            }
        })
    }

    fun logOutApp(view: View) {
        doAsync {
            db.userDao().logOut(false)
        }
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
    }

    fun createNewSite(view: View) {
        val intent = Intent(baseContext, CreateSite::class.java)
        startActivity(intent)
    }

}