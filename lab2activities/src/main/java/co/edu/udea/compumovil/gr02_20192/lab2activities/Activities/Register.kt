package co.edu.udea.compumovil.gr02_20192.lab2activities.Activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import co.edu.udea.compumovil.gr02_20192.lab2activities.DataBase.AppDataBase
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.POI
import co.edu.udea.compumovil.gr02_20192.lab2activities.Entity.User
import co.edu.udea.compumovil.gr02_20192.lab2activities.R
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.doAsync
import java.io.ByteArrayOutputStream

class Register : AppCompatActivity() {

    private val db = AppDataBase.getDataBase(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    fun registerUser(view: View) {
        var userData: User?
        doAsync {
            userData = db.userDao().getUserDuplicate(name.text.toString())
            runOnUiThread {
                if (userData != null) {
                    Toast.makeText(
                        baseContext,
                        R.string.a_user_with_that_name_already_exists,
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    saveData()
                }
            }
        }
    }

    fun saveData() {
        var userSave: User?
        if (email.text.toString() != "" && name.text.toString() != "" && password.text.toString() != "") {
            val user =
                User(
                    email.text.toString(),
                    name.text.toString(),
                    password.text.toString(),
                    true
                )
            doAsync {
                db.userDao().insertUser(user)
                userSave = db.userDao().autoLoguin(true)

                val drawable1: Drawable = resources.getDrawable(R.drawable.templo_sensoji)
                val bitmap1 = (drawable1 as BitmapDrawable).bitmap
                val stream1 = ByteArrayOutputStream()
                bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, stream1)
                val bitMapData1 = stream1.toByteArray()
                val site1 = POI(
                    bitMapData1,
                    "Templo Senoji",
                    "Se trata del templo budista más antiguo que ver en Tokio y está dedicado a Kannon, el Dios de la misericordia",
                    "5",
                    userSave!!.codigo
                )
                db.poiDao().insertPoi(site1)

                val drawable2: Drawable = resources.getDrawable(R.drawable.cruce_de_shibuya)
                val bitmap2 = (drawable2 as BitmapDrawable).bitmap
                val stream2 = ByteArrayOutputStream()
                bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, stream2)
                val bitMapData2 = stream2.toByteArray()
                val site2 = POI(
                    bitMapData2,
                    "Cruce Shibuya",
                    "Se trata del cruce de pasos de peatones más abarrotado del mundo y te recomendamos que antes de verlo desde arriba, lo vivas",
                    "4",
                    userSave!!.codigo
                )
                db.poiDao().insertPoi(site2)

                val drawable3: Drawable = resources.getDrawable(R.drawable.calle_takeshita)
                val bitmap3 = (drawable3 as BitmapDrawable).bitmap
                val stream3 = ByteArrayOutputStream()
                bitmap3.compress(Bitmap.CompressFormat.JPEG, 100, stream3)
                val bitMapData3 = stream3.toByteArray()
                val site3 = POI(
                    bitMapData3,
                    "Calle Takeshita",
                    "Takeshita es una calle comercial en la que abundan negocios de todo tipo: ropa, ‘todo a 100’",
                    "4",
                    userSave!!.codigo
                )
                db.poiDao().insertPoi(site3)

                val drawable4: Drawable = resources.getDrawable(R.drawable.shinjuku)
                val bitmap4 = (drawable4 as BitmapDrawable).bitmap
                val stream4 = ByteArrayOutputStream()
                bitmap4.compress(Bitmap.CompressFormat.JPEG, 100, stream4)
                val bitMapData4 = stream4.toByteArray()
                val site4 = POI(
                    bitMapData4,
                    "Shinjuku",
                    "Shinjuku es uno de los barrios más intensos que ver en Tokio: karaokes, neones, salas de videojuegos, restaurantes por doquier, cines, centros comerciales",
                    "5",
                    userSave!!.codigo
                )
                db.poiDao().insertPoi(site4)

                val drawable5: Drawable = resources.getDrawable(R.drawable.akihabara)
                val bitmap5 = (drawable5 as BitmapDrawable).bitmap
                val stream5 = ByteArrayOutputStream()
                bitmap5.compress(Bitmap.CompressFormat.JPEG, 100, stream5)
                val bitMapData5 = stream5.toByteArray()
                val site5 = POI(
                    bitMapData5,
                    "Akihabara",
                    "El barrio electrónico, al que ir a jugar a videojuegos, ir de compras e ir a pasárselo bien",
                    "5",
                    userSave!!.codigo
                )
                db.poiDao().insertPoi(site5)
                runOnUiThread {
                    openFataInfo()
                }
            }
        } else {
            Toast.makeText(this, R.string.missingDateToEnter, Toast.LENGTH_SHORT).show()
        }
    }

    fun openFataInfo() {
        val intent = Intent(this, DataInformation::class.java)
        startActivity(intent)
    }
}

