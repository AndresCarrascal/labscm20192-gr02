package co.edu.udea.compumovil.gr02_20192.lab2activities.Entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "users")
class User(
    val correo: String,
    val usuario: String,
    val contraseña: String,
    val activo: Boolean,
    @PrimaryKey(autoGenerate = true)
    val codigo: Int = 0
) : Serializable