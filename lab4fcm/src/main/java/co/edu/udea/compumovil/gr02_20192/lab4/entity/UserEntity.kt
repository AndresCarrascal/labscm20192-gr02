package co.edu.udea.compumovil.gr02_20192.lab4.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
class UserEntity(
    val user: String,
    val password: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
)