package co.edu.udea.compumovil.gr02_20192.lab4.windows

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import co.edu.udea.compumovil.gr02_20192.lab4.MainActivity
import co.edu.udea.compumovil.gr02_20192.lab4.R
import co.edu.udea.compumovil.gr02_20192.lab4.viewModel.UserViewModel
import org.jetbrains.anko.doAsync

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val background = object : Thread() {
            override fun run() {
                try {
                    sleep(2000)
                    doAsync {
                        val userViewModel =
                            ViewModelProvider(this@SplashActivity).get(UserViewModel::class.java)
                        var data = userViewModel.getUserLogged()
                        runOnUiThread {
                            if (data != null) {
                                val intent = Intent(baseContext, MainActivity::class.java)
                                startActivity(intent)
                            } else {
                                val intent = Intent(baseContext, LoginActivity::class.java)
                                startActivity(intent)
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        background.start()
    }
}
