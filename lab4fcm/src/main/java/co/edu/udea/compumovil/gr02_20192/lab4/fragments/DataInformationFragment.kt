package co.edu.udea.compumovil.gr02_20192.lab4.fragments


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.edu.udea.compumovil.gr02_20192.lab4.R
import co.edu.udea.compumovil.gr02_20192.lab4.adapter.SitesAdapter
import co.edu.udea.compumovil.gr02_20192.lab4.viewModel.PoiViewModel
import kotlinx.android.synthetic.main.fragment_data_information.*


/**
 * A simple [Fragment] subclass.
 */
class DataInformationFragment : Fragment() {
    private lateinit var poiViewModel: PoiViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        poiViewModel = ViewModelProvider(this).get(PoiViewModel::class.java)
        val contexto: Context? = activity
        if (contexto != null) {
            if (isNetworkConnected(contexto)) {
                poiViewModel.synchronizeInformation()
            } else {
                Toast.makeText(
                    contexto,
                    R.string.dontConnectionData,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        permissionWarning()
        addList()
        return inflater.inflate(R.layout.fragment_data_information, container, false)
    }

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun addList() {
        val contexto: Context = activity!!
        poiViewModel.getAllPois().observe(viewLifecycleOwner, Observer {
            val adapter = SitesAdapter(contexto, it)
            myList.adapter = adapter
            myList.setOnItemClickListener { parent, view, position, id ->
                poiViewModel = activity?.run {
                    ViewModelProvider(this)[PoiViewModel::class.java]
                } ?: throw Exception("Invalid Activity")
                poiViewModel.select(it[position])
                findNavController().navigate(R.id.detailFragment)
            }
        })
    }

    fun permissionWarning() {
        val contexto: Context? = activity
        if (contexto != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(
                        contexto,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    Toast.makeText(contexto, R.string.checkPermission, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}
