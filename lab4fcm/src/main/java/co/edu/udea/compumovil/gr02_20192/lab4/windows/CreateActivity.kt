package co.edu.udea.compumovil.gr02_20192.lab4.windows

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import co.edu.udea.compumovil.gr02_20192.lab4.MainActivity
import co.edu.udea.compumovil.gr02_20192.lab4.R
import co.edu.udea.compumovil.gr02_20192.lab4.entity.PoiEntity
import co.edu.udea.compumovil.gr02_20192.lab4.viewModel.PoiViewModel
import co.edu.udea.compumovil.gr02_20192.lab4.viewModel.UserViewModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_create.*
import org.jetbrains.anko.doAsync

class CreateActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    private lateinit var poiViewModel: PoiViewModel
    private lateinit var userViewModel: UserViewModel
    private var idSelect: Int = 0
    private var imageSelect: String? = null
    private var thumbnailSelect: String? = null
    private var haveConnection: Boolean = false
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var googleApliClient: GoogleApiClient

    companion object {
        private val PERMISSION_CODE = 1001
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        poiViewModel = ViewModelProvider(this).get(PoiViewModel::class.java)
        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create)
        if (isNetworkConnected(this)) {
            haveConnection = true
            poiViewModel.synchronizeInformation()
        }
        firebaseAuth = FirebaseAuth.getInstance()
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        googleApliClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
    }

    fun createSite(view: View) {
        if (name.text.toString() != "" && imageSelect != null && thumbnailSelect != null && description.text.toString() != "") {
            if (haveConnection) {
                val poi =
                    PoiEntity(
                        imageSelect!!,
                        thumbnailSelect!!,
                        description.text.toString(),
                        name.text.toString(),
                        true
                    )
                poiViewModel.insertPoiWhitConexion(poi)
            } else {
                val poi =
                    PoiEntity(
                        imageSelect!!,
                        thumbnailSelect!!,
                        description.text.toString(),
                        name.text.toString(),
                        false
                    )
                poiViewModel.insertPoiWithoutConexion(poi)
            }
            val intent = Intent(baseContext, MainActivity::class.java)
            startActivity(intent)
        } else {
            Toast.makeText(
                this,
                R.string.missing_date_to_enter,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun addImage(view: View) {
        idSelect = view.id
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSION_CODE)
            } else {
                intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                intent.type = "image/"
                startActivityForResult(
                    Intent.createChooser(
                        intent,
                        R.string.select_an_image.toString()
                    ), 10
                )
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (idSelect == thumbnail.id) {
                textThumbnail.setTextColor(Color.parseColor("#FFFFFF"))
                textThumbnail.setText(R.string.thumbnail_select)
                thumbnailSelect = getRealPathFromURI(data?.data!!, this)
            } else {
                textImage.setTextColor(Color.parseColor("#FFFFFF"))
                textImage.setText(R.string.select_image)
                imageSelect = getRealPathFromURI(data?.data!!, this)
            }
        }
    }

    fun getRealPathFromURI(contentUri: Uri, activityContext: Activity): String {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = activityContext.managedQuery(contentUri, proj, null, null, null)
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intent =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    intent.type = "image/"
                    startActivityForResult(
                        Intent.createChooser(
                            intent,
                            R.string.select_an_image.toString()
                        ), 10
                    )
                } else {
                    Toast.makeText(this, R.string.accept_permission, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menuopciones, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val intent = Intent(baseContext, LoginActivity::class.java)
        val id = item.itemId
        if (id == R.id.opcion1) {
            doAsync {
                val userLogued = userViewModel.getUserLogged()
                runOnUiThread {
                    if (userLogued != null) {
                        userViewModel.deleteUserLogged()
                        startActivity(intent)
                    } else {
                        firebaseAuth.signOut()
                        Auth.GoogleSignInApi.revokeAccess(googleApliClient).setResultCallback {
                            if (it.isSuccess) {
                                startActivity(intent)
                            } else {
                                Log.d("ERROR", "No se pudo cerrar la sesión")
                            }
                        }
                    }
                }
            }
            Toast.makeText(this, R.string.session_closed, Toast.LENGTH_SHORT).show()
            return true
        }
        if (id == R.id.permissions) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                    requestPermissions(permissions, PERMISSION_CODE)
                } else {
                    Toast.makeText(
                        this,
                        R.string.necessaryPermissions,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}