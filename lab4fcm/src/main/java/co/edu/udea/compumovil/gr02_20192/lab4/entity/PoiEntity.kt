package co.edu.udea.compumovil.gr02_20192.lab4.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "pois")
class PoiEntity(
    val image: String,
    val thumbnail: String,
    val description: String,
    val title: String,
    val sync: Boolean,
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
) : Serializable