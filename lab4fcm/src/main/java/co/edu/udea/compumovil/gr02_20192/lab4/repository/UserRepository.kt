package co.edu.udea.compumovil.gr02_20192.lab4.repository

import androidx.lifecycle.LiveData
import co.edu.udea.compumovil.gr02_20192.lab4.MyAplication
import co.edu.udea.compumovil.gr02_20192.lab4.entity.UserEntity
import co.edu.udea.compumovil.gr02_20192.lab4.entity.UserLoggedEntity

class UserRepository {

    private val db = MyAplication.db!!

    fun getUserLogged(): UserLoggedEntity {
        return db.userLoggedDao().getUserLogged()
    }

    fun saveUserLogged(userLoggedEntity: UserLoggedEntity) {
        db.userLoggedDao().insertUserLogged(userLoggedEntity)
    }

    fun deleteUserLogged() {
        db.userLoggedDao().deleteAllUserLogged()
    }

    fun getOneForPasswordAndUser(password: String, user: String): LiveData<UserEntity> {
        return db.userDao().getOneForPasswordAndUser(password, user)
    }

    fun insertUser(user: UserEntity) {
        return db.userDao().insertUser(user)
    }
}