package co.edu.udea.compumovil.gr02_20192.lab4.windows

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.edu.udea.compumovil.gr02_20192.lab4.MainActivity
import co.edu.udea.compumovil.gr02_20192.lab4.R
import co.edu.udea.compumovil.gr02_20192.lab4.entity.UserLoggedEntity
import co.edu.udea.compumovil.gr02_20192.lab4.viewModel.UserViewModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    private lateinit var userViewModel: UserViewModel
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var firebaseAuthListener: FirebaseAuth.AuthStateListener
    private lateinit var googleApliClient: GoogleApiClient

    companion object {
        private val SING_IN_CODE = 777
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        googleApliClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()


        signInButton.setOnClickListener {
            val intent = Auth.GoogleSignInApi.getSignInIntent(googleApliClient)
            startActivityForResult(intent, SING_IN_CODE)
        }

        firebaseAuth = FirebaseAuth.getInstance()
        firebaseAuthListener = FirebaseAuth.AuthStateListener {
            val user = firebaseAuth.currentUser
            if (user != null) {
                val intent = Intent(baseContext, MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        firebaseAuth.addAuthStateListener(firebaseAuthListener)
    }

    fun createAccount(view: View) {
        val intent = Intent(baseContext, RegisterActivity::class.java)
        startActivity(intent)
    }

    fun login(view: View) {
        userViewModel.getOneForPasswordAndUser(password.text.toString(), name.text.toString())
            .observe(this, Observer {
                if (it != null) {
                    val userLogged =
                        UserLoggedEntity(
                            it.user,
                            it.password,
                            it.id
                        )
                    userViewModel.saveUserLogged(userLogged)
                    val intent = Intent(baseContext, MainActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(
                        this,
                        R.string.username_does_not_exist,
                        Toast.LENGTH_LONG
                    ).show()
                }
            })
    }


    override fun onStop() {
        super.onStop()
        if (firebaseAuthListener != null) {
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SING_IN_CODE) {
            var result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSingInResult(result)
        }
    }

    private fun handleSingInResult(result: GoogleSignInResult?) {
        if (result != null) {
            if (result.isSuccess) {
                firebaseAuthWhitGoogle(result.signInAccount)
                val intent = Intent(baseContext, MainActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(
                    this,
                    R.string.cannotLogin,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun firebaseAuthWhitGoogle(signInAccount: GoogleSignInAccount?) {
        val credential: AuthCredential =
            GoogleAuthProvider.getCredential(signInAccount?.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this) { p0 ->
            if (p0.isSuccessful) {
                Toast.makeText(
                    applicationContext,
                    R.string.cannotAuthFirebase,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}
