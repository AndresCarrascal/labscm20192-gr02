package co.edu.udea.compumovil.gr02_20192.lab4.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import co.edu.udea.compumovil.gr02_20192.lab4.entity.PoiEntity

@Dao
interface PoiDao {
    @Insert
    fun insertPoi(poi: PoiEntity): Long

    @Update
    fun updatePoi(poi: PoiEntity)

    @Query("SELECT * FROM pois")
    fun getAll(): LiveData<List<PoiEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllpois(poisList: List<PoiEntity>)

    @Query("SELECT * FROM pois WHERE sync = :value")
    fun finAllWithoutConexion(value: Boolean): List<PoiEntity>
}