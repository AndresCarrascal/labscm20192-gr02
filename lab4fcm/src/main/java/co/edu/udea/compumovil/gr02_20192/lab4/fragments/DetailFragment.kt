package co.edu.udea.compumovil.gr02_20192.lab4.fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.edu.udea.compumovil.gr02_20192.lab4.R
import co.edu.udea.compumovil.gr02_20192.lab4.entity.PoiEntity
import co.edu.udea.compumovil.gr02_20192.lab4.viewModel.PoiViewModel
import com.squareup.picasso.Picasso
import java.io.File
import java.net.MalformedURLException
import java.net.URL

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {
    private lateinit var title_: TextView
    private lateinit var description_: TextView
    private lateinit var imageView_: ImageView
    private lateinit var poiViewModel: PoiViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        poiViewModel = ViewModelProvider(this).get(PoiViewModel::class.java)
        val view = inflater.inflate(R.layout.fragment_data_information_detail, container, false)
        title_ = view.findViewById(R.id.name)
        description_ = view.findViewById(R.id.description)
        imageView_ = view.findViewById(R.id.imageViewDetail)

        poiViewModel = activity?.run {
            ViewModelProviders.of(this)[PoiViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        poiViewModel.selected.observe(viewLifecycleOwner, Observer<PoiEntity> { poi ->
            if(isURL(poi.image)){
                Picasso.get().load(poi.image)
                    .error(R.drawable.error_icon)
                    .resize(550, 413)
                    .centerCrop()
                    .into(imageView_)
            } else {
                val file = File(poi.image)
                Picasso.get().load(file)
                    .error(R.drawable.error_icon)
                    .resize(550, 413)
                    .centerCrop()
                    .into(imageView_)
            }
            title_.text = poi.title
            description_.text = poi.description
        })
        return view
    }

    fun isURL(urlP: String): Boolean {
        try {
            URL(urlP)
            return true
        } catch (e: MalformedURLException) {
            Log.e("URL Test Fail", e.message)
        }
        return false
    }

}
