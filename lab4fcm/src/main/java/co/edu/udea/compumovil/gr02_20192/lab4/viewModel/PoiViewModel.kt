package co.edu.udea.compumovil.gr02_20192.lab4.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.edu.udea.compumovil.gr02_20192.lab4.entity.PoiEntity
import co.edu.udea.compumovil.gr02_20192.lab4.repository.PoiRepository
import kotlinx.coroutines.*

class PoiViewModel : ViewModel() {

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    var poiRepository: PoiRepository = PoiRepository()

    val selected = MutableLiveData<PoiEntity>()

    fun select(item: PoiEntity) {
        selected.value = item
    }

    fun getAllPois(): LiveData<List<PoiEntity>> {
        return poiRepository.getAllPois()
    }

    fun synchronizeInformation() {
        poiRepository.synchronizeInformation()
    }

    fun insertPoiWhitConexion(poi: PoiEntity) {
        uiScope.launch {
            insertPoiWhitConexion_(poi)
        }
    }

    private suspend fun insertPoiWhitConexion_(poi: PoiEntity) {
        withContext(Dispatchers.IO) {
            poiRepository.insertPoiWhitConexion(poi)
        }
    }

    fun insertPoiWithoutConexion(poi: PoiEntity) {
        uiScope.launch {
            insertPoiWithoutConexion_(poi)
        }
    }

    private suspend fun insertPoiWithoutConexion_(poi: PoiEntity) {
        withContext(Dispatchers.IO) {
            poiRepository.insertPoiWithoutConexion(poi)
        }
    }
}