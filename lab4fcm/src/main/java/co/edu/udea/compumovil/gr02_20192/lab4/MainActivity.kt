package co.edu.udea.compumovil.gr02_20192.lab4

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import co.edu.udea.compumovil.gr02_20192.lab4.viewModel.UserViewModel
import co.edu.udea.compumovil.gr02_20192.lab4.windows.CreateActivity
import co.edu.udea.compumovil.gr02_20192.lab4.windows.LoginActivity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync

class MainActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    private lateinit var userViewModel: UserViewModel
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var googleApliClient: GoogleApiClient

    companion object {
        private val PERMISSION_CODE = 1001
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        val navController = this.findNavController(R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController)
        firebaseAuth = FirebaseAuth.getInstance()
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        googleApliClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.nav_host_fragment)
        return navController.navigateUp()
    }

    fun createActivity(view: View) {
        val intent = Intent(baseContext, CreateActivity::class.java)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menuopciones, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val intent = Intent(baseContext, LoginActivity::class.java)
        val id = item.itemId
        if (id == R.id.opcion1) {
            doAsync {
                val userLogued = userViewModel.getUserLogged()
                runOnUiThread {
                    if (userLogued != null) {
                        userViewModel.deleteUserLogged()
                        startActivity(intent)
                    } else {
                        firebaseAuth.signOut()
                        Auth.GoogleSignInApi.revokeAccess(googleApliClient).setResultCallback {
                            if (it.isSuccess) {
                                startActivity(intent)
                            } else {
                                Log.d("ERROR", "No se pudo cerrar la sesión")
                            }
                        }
                    }
                }
            }
            Toast.makeText(this, R.string.session_closed, Toast.LENGTH_SHORT).show()
            return true
        }
        if (id == R.id.permissions) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                    requestPermissions(permissions, PERMISSION_CODE)
                } else {
                    Toast.makeText(
                        this,
                        R.string.necessaryPermissions,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish()
                    startActivity(intent)
                } else {
                    Toast.makeText(this, R.string.permitsNotAccepted, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}
