package co.edu.udea.compumovil.gr02_20192.lab4

import android.app.Application
import co.edu.udea.compumovil.gr02_20192.lab4.dataBaseRoom.AppDataBase
import com.google.firebase.FirebaseApp

class MyAplication : Application() {

    companion object {
        var db: AppDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        db =
            AppDataBase.getDataBase(
                this
            )
    }

}