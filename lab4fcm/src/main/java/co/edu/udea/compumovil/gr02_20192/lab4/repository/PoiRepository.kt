package co.edu.udea.compumovil.gr02_20192.lab4.repository

import androidx.lifecycle.LiveData
import co.edu.udea.compumovil.gr02_20192.lab4.MyAplication
import co.edu.udea.compumovil.gr02_20192.lab4.entity.PoiEntity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.doAsync

class PoiRepository {

    private val db = MyAplication.db!!
    val database = FirebaseDatabase.getInstance().reference
    val poiList = ArrayList<PoiEntity>()

    fun getAllPois(): LiveData<List<PoiEntity>> {
        return db.poiDao().getAll()
    }

    fun insertPoiWhitConexion(poi: PoiEntity) {
        val porId = db.poiDao().insertPoi(poi)
        val poiFirebase =
            PoiEntity(
                poi.image,
                poi.thumbnail,
                poi.description,
                poi.title,
                poi.sync,
                porId.toInt()
            )
        database.child("pois").push().setValue(poiFirebase)
    }

    fun insertPoiWithoutConexion(poi: PoiEntity) {
        db.poiDao().insertPoi(poi)
    }

    fun synchronizeInformation() {
        syncSQLite()
    }

    fun syncSQLite() {
        database.child("pois").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val poi =
                        PoiEntity(
                            it.child("image").value.toString(),
                            it.child("thumbnail").value.toString(),
                            it.child("description").value.toString(),
                            it.child("title").value.toString(),
                            it.child("sync").value.toString().toBoolean(),
                            it.child("id").value.toString().toInt()
                        )
                    poiList.add(poi)
                }
                Thread(Runnable {
                    if (poiList.isNotEmpty()) {
                        db.poiDao().insertAllpois(poiList)
                    }
                }).start()
                syncFirebase()
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    fun syncFirebase() {
        doAsync {
            var newPoiList = ArrayList<PoiEntity>()
            var poiList: List<PoiEntity> = db.poiDao().finAllWithoutConexion(false)
            poiList.forEach {
                val poi =
                    PoiEntity(
                        it.image,
                        it.thumbnail,
                        it.description,
                        it.title,
                        true,
                        it.id
                    )
                newPoiList.add(poi)
            }
            if (newPoiList.isNotEmpty()) {
                newPoiList.forEach {
                    db.poiDao().updatePoi(it)
                    database.child("pois").push().setValue(it)
                }
            }
        }
    }
}