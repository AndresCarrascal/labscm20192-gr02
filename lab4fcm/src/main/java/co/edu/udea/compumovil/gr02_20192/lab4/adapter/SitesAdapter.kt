package co.edu.udea.compumovil.gr02_20192.lab4.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import co.edu.udea.compumovil.gr02_20192.lab4.R
import co.edu.udea.compumovil.gr02_20192.lab4.entity.PoiEntity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.items_sites.view.*
import java.io.File
import java.net.MalformedURLException
import java.net.URL


class SitesAdapter(private val mContext: Context, private val listSites: List<PoiEntity>) :
    ArrayAdapter<PoiEntity>(mContext, 0, listSites) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.items_sites, parent, false)
        val sites = listSites[position]
        if(isURL(sites.thumbnail)){
            Picasso.get().load(sites.thumbnail)
                .error(R.drawable.error_icon)
                .resize(50, 50)
                .centerCrop()
                .into(layout.myImageView)
        } else {
            val file = File(sites.thumbnail)
            Picasso.get().load(file)
                .error(R.drawable.error_icon)
                .resize(50, 50)
                .centerCrop()
                .into(layout.myImageView)
        }
        layout.name.text = sites.title
        return layout
    }

    fun isURL(urlP: String): Boolean {
        try {
            URL(urlP)
            return true
        } catch (e: MalformedURLException) {
            Log.e("URL Test Fail", e.message)
        }
        return false
    }
}