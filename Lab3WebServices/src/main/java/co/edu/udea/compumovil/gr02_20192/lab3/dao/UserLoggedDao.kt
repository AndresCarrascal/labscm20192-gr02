package co.edu.udea.compumovil.gr02_20192.lab3.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserLoggedEntity

@Dao
interface UserLoggedDao {
    @Query("SELECT * FROM user_logged LIMIT 1")
    fun getUserLogged(): UserLoggedEntity
    @Insert
    fun insertUserLogged(userLogged: UserLoggedEntity)

    @Query("DELETE FROM user_logged")
    fun deleteAllUserLogged()
}