package co.edu.udea.compumovil.gr02_20192.lab3.repository

import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.LiveData
import co.edu.udea.compumovil.gr02_20192.lab3.MyAplication
import co.edu.udea.compumovil.gr02_20192.lab3.api.RetrofitFactory
import co.edu.udea.compumovil.gr02_20192.lab3.entity.PoiEntity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PoiRepository {

    private val db = MyAplication.db!!

    fun getAllPois(): LiveData<List<PoiEntity>> {
        return db.poiDao().getAll()
    }

    fun insertPoi(poi: PoiEntity) {
        return db.poiDao().insertPoi(poi)
    }

    fun apiCallAndPutInDB() {
        val apiService = RetrofitFactory.poiService()
        apiService.requestPois().enqueue(object : Callback<List<PoiEntity>> {
            override fun onFailure(call: Call<List<PoiEntity>>?, t: Throwable?) {
                Log.e("ERROR", "OOPS!! something went wrong..")
            }

            override fun onResponse(
                call: Call<List<PoiEntity>>,
                response: Response<List<PoiEntity>>
            ) {
                Log.e(ContentValues.TAG, response.body().toString())

                when (response.code()) {
                    200 -> {
                        Thread(Runnable {
                            db.poiDao().insertAllpois(response.body()!!)
                        }).start()
                    }
                }
            }

        })
    }
}