package co.edu.udea.compumovil.gr02_20192.lab3.dataBaseRoom

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import co.edu.udea.compumovil.gr02_20192.lab3.dao.PoiDao
import co.edu.udea.compumovil.gr02_20192.lab3.dao.UserDao
import co.edu.udea.compumovil.gr02_20192.lab3.dao.UserLoggedDao
import co.edu.udea.compumovil.gr02_20192.lab3.entity.PoiEntity
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserEntity
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserLoggedEntity

@Database(entities = [UserEntity::class, UserLoggedEntity::class, PoiEntity::class], version = 1)
abstract class AppDataBase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun userLoggedDao(): UserLoggedDao
    abstract fun poiDao(): PoiDao

    companion object {
        @Volatile
        private var INSTANCE: AppDataBase? = null

        fun getDataBase(context: Context): AppDataBase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDataBase::class.java,
                    "app_database"
                ).build()

                INSTANCE = instance
                return instance
            }

        }
    }
}