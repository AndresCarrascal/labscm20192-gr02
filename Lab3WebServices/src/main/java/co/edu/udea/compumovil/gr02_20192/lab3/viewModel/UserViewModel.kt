package co.edu.udea.compumovil.gr02_20192.lab3.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserEntity
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserLoggedEntity
import co.edu.udea.compumovil.gr02_20192.lab3.repository.UserRepository
import kotlinx.coroutines.*

class UserViewModel : ViewModel() {

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    var userRepository: UserRepository = UserRepository()

    fun getOneForPasswordAndUser(password: String, user: String): LiveData<UserEntity> {
        return userRepository.getOneForPasswordAndUser(password, user)
    }

    fun apiCallAndPutInDb() {
        userRepository.apiCallAndPutInDB()
    }

    fun getUserLogged(): UserLoggedEntity {
        return userRepository.getUserLogged()
    }


    //---------------------------------------------------------------------------------------------------------//

    fun saveUserLogged(userLoggedEntity: UserLoggedEntity) {
        uiScope.launch {
            saveUserLogged_(userLoggedEntity);
        }
    }

    private suspend fun saveUserLogged_(userLoggedEntity: UserLoggedEntity) {
        return withContext(Dispatchers.IO) {
            userRepository.saveUserLogged(userLoggedEntity)
        }
    }

    fun insertUser(user: UserEntity) {
        uiScope.launch {
            insertUser_(user)
        }
    }

    private suspend fun insertUser_(user: UserEntity) {
        withContext(Dispatchers.IO) {
            userRepository.insertUser(user)
        }
    }

    fun deleteUserLogged() {
        uiScope.launch {
            deleteUserLogged_()
        }
    }

    private suspend fun deleteUserLogged_() {
        return withContext(Dispatchers.IO) {
            userRepository.saveDeleteUserLogged()
        }
    }

}