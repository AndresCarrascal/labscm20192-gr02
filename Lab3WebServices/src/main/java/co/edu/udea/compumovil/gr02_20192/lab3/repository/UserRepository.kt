package co.edu.udea.compumovil.gr02_20192.lab3.repository

import android.app.Application
import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.LiveData
import co.edu.udea.compumovil.gr02_20192.lab3.api.RetrofitFactory
import co.edu.udea.compumovil.gr02_20192.lab3.dataBaseRoom.AppDataBase
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserEntity
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserLoggedEntity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository : Application() {

    private val db = AppDataBase.getDataBase(this)

    fun getUserLogged(): UserLoggedEntity {
        return db.userLoggedDao().getUserLogged()
    }

    fun saveUserLogged(userLoggedEntity: UserLoggedEntity) {
        db.userLoggedDao().insertUserLogged(userLoggedEntity)
    }

    fun saveDeleteUserLogged() {
        db.userLoggedDao().deleteAllUserLogged()
    }

    fun getUsers(): LiveData<List<UserEntity>> {
        return db.userDao().getUsers()
    }

    fun getOneForPasswordAndUser(password: String, user: String): LiveData<UserEntity> {
        return db.userDao().getOneForPasswordAndUser(password, user)
    }

    fun insertUser(user: UserEntity) {
        return db.userDao().insertUser(user)
    }

    fun apiCallAndPutInDB() {
        val apiService = RetrofitFactory.userService()
        apiService.requestUsers().enqueue(object : Callback<List<UserEntity>> {
            override fun onFailure(call: Call<List<UserEntity>>?, t: Throwable?) {
                Log.e("ERROR", "OOPS!! something went wrong..")
            }

            override fun onResponse(
                call: Call<List<UserEntity>>,
                response: Response<List<UserEntity>>
            ) {
                Log.e(TAG, response!!.body().toString())

                when (response.code()) {
                    200 -> {
                        Thread(Runnable {

                            db.userDao().insertAllUsers(response.body()!!)

                        }).start()
                    }
                }
            }

        })
    }
}