package co.edu.udea.compumovil.gr02_20192.lab3.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserEntity

@Dao
interface UserDao {
    @Insert
    fun insertUser(user: UserEntity)

    @Update
    fun updateUser(user: UserEntity)

    @Query("SELECT * FROM users")
    fun getUsers(): LiveData<List<UserEntity>>

    @Query("SELECT * FROM users WHERE id = :id")
    fun getOne(id: Int): UserEntity

    @Delete
    fun delete(user: UserEntity)

    @Query("SELECT * FROM users WHERE password = :password AND user = :user LIMIT 1")
    fun getOneForPasswordAndUser(password: String, user: String): LiveData<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllUsers(usersList: List<UserEntity>)
}