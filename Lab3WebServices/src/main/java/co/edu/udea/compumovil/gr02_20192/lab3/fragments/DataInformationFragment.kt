package co.edu.udea.compumovil.gr02_20192.lab3.fragments


import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.edu.udea.compumovil.gr02_20192.lab3.R
import co.edu.udea.compumovil.gr02_20192.lab3.adapter.SitesAdapter
import co.edu.udea.compumovil.gr02_20192.lab3.viewModel.PoiViewModel
import kotlinx.android.synthetic.main.fragment_data_information.*


/**
 * A simple [Fragment] subclass.
 */
class DataInformationFragment : Fragment() {
    private lateinit var poiViewModel: PoiViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        poiViewModel = ViewModelProvider(this).get(PoiViewModel::class.java)
        val contexto: Context? = activity
        if (contexto != null) {
            if (isNetworkConnected(contexto)) {
                poiViewModel.apiCallAndPutInDb()
            } else {
                Toast.makeText(
                    contexto,
                    R.string.dont_conection.toString(),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        addList()
        return inflater.inflate(R.layout.fragment_data_information, container, false)
    }

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun addList() {
        val contexto: Context = activity!!
        poiViewModel.getAllPois().observe(viewLifecycleOwner, Observer {
            val adapter = SitesAdapter(contexto, it)
            myList.adapter = adapter
            myList.setOnItemClickListener { parent, view, position, id ->
                poiViewModel = activity?.run {
                    ViewModelProvider(this)[PoiViewModel::class.java]
                } ?: throw Exception("Invalid Activity")
                poiViewModel.select(it[position])
                findNavController().navigate(R.id.detailFragment)
            }
        })
    }


}
