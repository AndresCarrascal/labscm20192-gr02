package co.edu.udea.compumovil.gr02_20192.lab3.windows

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import co.edu.udea.compumovil.gr02_20192.lab3.MainActivity
import co.edu.udea.compumovil.gr02_20192.lab3.R
import co.edu.udea.compumovil.gr02_20192.lab3.entity.PoiEntity
import co.edu.udea.compumovil.gr02_20192.lab3.viewModel.PoiViewModel
import co.edu.udea.compumovil.gr02_20192.lab3.viewModel.UserViewModel
import kotlinx.android.synthetic.main.activity_create.*

class CreateActivity : AppCompatActivity() {

    private lateinit var poiViewModel: PoiViewModel
    private lateinit var userViewModel: UserViewModel
    private var idSelect: Int = 0
    private var imageSelect: String? =  null
    private var thumbnailSelect: String? =  null
    companion object {
        private val PERMISSION_CODE = 1001
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        poiViewModel = ViewModelProvider(this).get(PoiViewModel::class.java)
        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create)
    }

    fun createSite(view: View) {
        if (name.text.toString() != "" && imageSelect != null && thumbnailSelect != null && description.text.toString() != "") {
            val poi =
                PoiEntity(
                    imageSelect!!,
                    thumbnailSelect!!,
                    description.text.toString(),
                    name.text.toString()
                )
            poiViewModel.insertPoi(poi)
            val intent = Intent(baseContext, MainActivity::class.java)
            startActivity(intent)
        } else {
            Toast.makeText(
                this,
                R.string.missing_date_to_enter,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun addImage(view: View) {
        idSelect = view.id
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSION_CODE)
            } else {
                intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                intent.type = "image/"
                startActivityForResult(Intent.createChooser(intent, R.string.select_an_image.toString()), 10)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (idSelect == thumbnail.id) {
                textThumbnail.setTextColor(Color.parseColor("#FFFFFF"))
                textThumbnail.setText(R.string.thumbnail_select)
                thumbnailSelect = getRealPathFromURI(data?.data!!, this)
            } else {
                textImage.setTextColor(Color.parseColor("#FFFFFF"))
                textImage.setText(R.string.select_image)
                imageSelect = getRealPathFromURI(data?.data!!, this)
            }
        }
    }

    fun getRealPathFromURI(contentUri: Uri, activityContext: Activity): String {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = activityContext.managedQuery(contentUri, proj, null, null, null)
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    intent.type = "image/"
                    startActivityForResult(Intent.createChooser(intent, R.string.select_an_image.toString()), 10)
                } else {
                    Toast.makeText(this, R.string.accept_permission, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menuopciones, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.opcion1) {
            Toast.makeText(this, R.string.session_closed, Toast.LENGTH_SHORT).show()
            userViewModel.deleteUserLogged()
            val intent = Intent(baseContext, LoginActivity::class.java)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}