package co.edu.udea.compumovil.gr02_20192.lab3.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.edu.udea.compumovil.gr02_20192.lab3.entity.PoiEntity
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserEntity
import co.edu.udea.compumovil.gr02_20192.lab3.repository.PoiRepository
import kotlinx.coroutines.*

class PoiViewModel : ViewModel() {

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    var poiRepository: PoiRepository = PoiRepository()

    val selected = MutableLiveData<PoiEntity>()

    fun select(item: PoiEntity) {
        selected.value = item
    }

    fun getAllPois(): LiveData<List<PoiEntity>> {
        return poiRepository.getAllPois()
    }

    fun apiCallAndPutInDb() {
        poiRepository.apiCallAndPutInDB()
    }

    fun insertPoi(poi: PoiEntity) {
        uiScope.launch {
            insertPoi_(poi)
        }
    }

    private suspend fun insertPoi_(poi: PoiEntity) {
        withContext(Dispatchers.IO) {
            poiRepository.insertPoi(poi)
        }
    }
}