package co.edu.udea.compumovil.gr02_20192.lab3.windows

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.edu.udea.compumovil.gr02_20192.lab3.MainActivity
import co.edu.udea.compumovil.gr02_20192.lab3.R
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserLoggedEntity
import co.edu.udea.compumovil.gr02_20192.lab3.viewModel.UserViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        if (isNetworkConnected(this)) {
            userViewModel.apiCallAndPutInDb()
        }
    }

    fun createAccount(view: View) {
        val intent = Intent(baseContext, RegisterActivity::class.java)
        startActivity(intent)
    }

    fun login(view: View) {
        userViewModel.getOneForPasswordAndUser(password.text.toString(), name.text.toString())
            .observe(this, Observer {
                if (it != null) {
                    val userLogged =
                        UserLoggedEntity(
                            it.user,
                            it.password,
                            it.id
                        )
                    userViewModel.saveUserLogged(userLogged)
                    val intent = Intent(baseContext, MainActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(
                        this,
                        R.string.username_does_not_exist,
                        Toast.LENGTH_LONG
                    ).show()
                }
            })
    }

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}
