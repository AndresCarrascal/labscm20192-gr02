package co.edu.udea.compumovil.gr02_20192.lab3.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitFactory {

    private const val BASE_URL = "https://my-json-server.typicode.com/AndresCarrascal/api/"

    private val gson: Gson = GsonBuilder()
        .setLenient()
        .create()

    private fun retrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    fun userService(): UserService = retrofit().create(UserService::class.java)


    fun poiService(): PoiService = retrofit().create(PoiService::class.java)
}
