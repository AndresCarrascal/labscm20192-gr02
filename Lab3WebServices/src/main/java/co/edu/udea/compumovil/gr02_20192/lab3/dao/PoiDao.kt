package co.edu.udea.compumovil.gr02_20192.lab3.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.edu.udea.compumovil.gr02_20192.lab3.entity.PoiEntity
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserEntity

@Dao
interface PoiDao {
    @Insert
    fun insertPoi(poi: PoiEntity)

    @Query("SELECT * FROM pois")
    fun getAll(): LiveData<List<PoiEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllpois(poisList: List<PoiEntity>)
}