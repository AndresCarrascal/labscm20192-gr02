package co.edu.udea.compumovil.gr02_20192.lab3.api

import co.edu.udea.compumovil.gr02_20192.lab3.entity.PoiEntity
import retrofit2.Call
import retrofit2.http.GET

interface PoiService {

    @GET("poi")
    fun requestPois(): Call<List<PoiEntity>>

}