package co.edu.udea.compumovil.gr02_20192.lab3

import android.app.Application
import co.edu.udea.compumovil.gr02_20192.lab3.dataBaseRoom.AppDataBase

class MyAplication : Application() {

    companion object {
        var db: AppDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()
        db =
            AppDataBase.getDataBase(
                this
            )
    }

}