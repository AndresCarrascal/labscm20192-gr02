package co.edu.udea.compumovil.gr02_20192.lab3.windows

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import co.edu.udea.compumovil.gr02_20192.lab3.R
import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserEntity
import co.edu.udea.compumovil.gr02_20192.lab3.viewModel.UserViewModel
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    fun registerUser(view: View) {
        if (name.text.toString() != "" && password.text.toString() != "") {
            val user =
                UserEntity(
                    name.text.toString(),
                    password.text.toString()
                )
            userViewModel.insertUser(user)
            val intent = Intent(baseContext, LoginActivity::class.java)
            startActivity(intent)
        } else {
            Toast.makeText(
                this,
                R.string.missing_date_to_enter,
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
