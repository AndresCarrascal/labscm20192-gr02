package co.edu.udea.compumovil.gr02_20192.lab3.api

import co.edu.udea.compumovil.gr02_20192.lab3.entity.UserEntity
import retrofit2.Call
import retrofit2.http.GET

interface UserService {

    @GET("users")
    fun requestUsers(): Call<List<UserEntity>>

}