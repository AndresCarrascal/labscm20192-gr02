package co.edu.udea.compumovil.gr02_20192.lab3.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_logged")
class UserLoggedEntity(
    val user: String,
    val password: String,
    val codigo: Int,
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
)