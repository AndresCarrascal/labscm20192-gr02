package co.edu.udea.compumovil.gr02_20192.lab1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AutoCompleteTextView
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_contact_info.*
import kotlinx.android.synthetic.main.activity_personal_info.*


class ContactInfo : AppCompatActivity() {
    private val COUNTRIES = arrayOf(
        "Argentina",
        "Bolivia",
        "Brasil",
        "Chile",
        "Colombia",
        "Costa Rica",
        "Cuba",
        "Ecuador",
        "El Salvador",
        "Guayana Francesa",
        "Granada",
        "Guatemala",
        "Guayana",
        "Haití",
        "Honduras",
        "Jamaica",
        "México",
        "Nicaragua",
        "Paraguay",
        "Panamá",
        "Perú",
        "Puerto Rico",
        "República Dominicana",
        "Surinam",
        "Uruguay",
        "Venezuela"
    )
    private val CITY = arrayOf(
        "Bogotá",
        "Medellín",
        "Cali",
        "Barranquilla",
        "Cartagena de indias",
        "Soledad",
        "Cúcuta",
        "Soacha",
        "Ibagué",
        "Bucaramanga",
        "Villavicencio",
        "Santa Marta",
        "Bello",
        "Valledupar",
        "Pereira",
        "Buenaventura",
        "Pasto",
        "Manizales",
        "Montería",
        "Neiva"
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_info)
        var adapterCountry = ArrayAdapter(this,android.R.layout.simple_list_item_1,COUNTRIES)
        autoCompleteTextViewCountry.threshold=0
        autoCompleteTextViewCountry.setAdapter(adapterCountry)
        autoCompleteTextViewCountry.setOnFocusChangeListener { _, b ->  if(b) autoCompleteTextViewCountry.showDropDown()}

        var adapterCity = ArrayAdapter(this,android.R.layout.simple_list_item_1,CITY)
        autoCompleteTextViewCity.threshold=0
        autoCompleteTextViewCity.setAdapter(adapterCity)
        autoCompleteTextViewCity.setOnFocusChangeListener { _, b ->  if(b) autoCompleteTextViewCity.showDropDown()}
    }

    fun validateData(view: View) {
        if (editTextEMail.text.toString() != "" && autoCompleteTextViewCountry.text.toString() != "" && autoCompleteTextViewCity.text.toString() != "" && editTextAddress.text.toString() != "" && editTextPhone.text.toString() != "") {
            Log.d("Telefono", editTextPhone.text.toString())
            Log.d("Correo", editTextEMail.text.toString())
            Log.d("País", autoCompleteTextViewCountry.text.toString())
            Log.d("Ciudad", autoCompleteTextViewCity.text.toString())
            Log.d("Dirección", editTextAddress.text.toString())
        } else {
            Log.d("ERROR", "Faltan datos por ingresar")
        }
    }
}
