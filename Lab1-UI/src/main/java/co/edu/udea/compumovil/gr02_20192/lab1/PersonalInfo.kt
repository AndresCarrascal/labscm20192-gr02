package co.edu.udea.compumovil.gr02_20192.lab1

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_personal_info.*
import java.util.*


class PersonalInfo : AppCompatActivity() {
    var day = 0
    var month = 0
    var year = 0
    lateinit var spinnerOptions: Spinner
    lateinit var radioButtonOptions: RadioGroup
    var date: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_info)
        spinnerOptions = findViewById(R.id.spinner)
        radioButtonOptions = findViewById(R.id.radioGroup)
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.gradoEscolaridad,
            android.R.layout.simple_spinner_item
        )
        spinnerOptions.adapter = adapter
    }

    @SuppressLint("SetTextI18n")
    fun funDate(view: View) {
        val c = Calendar.getInstance()
        day = c.get(Calendar.DAY_OF_MONTH)
        month = c.get(Calendar.MONTH)
        year = c.get(Calendar.YEAR)

        val dpd = DatePickerDialog(
            this,
            android.R.style.Theme_Holo_Dialog,
            DatePickerDialog.OnDateSetListener { datePicker, year, monthOfYear, dayOfMonth ->
                textViewDateOfBirth.text = "$dayOfMonth/$monthOfYear/$year"
                date = "$dayOfMonth/$monthOfYear/$year"
            },
            year,
            month,
            day
        )

        //show datepicker
        dpd.show()
    }

    fun validateData(view: View) {
        val id = radioButtonOptions.checkedRadioButtonId
        val radioButton = findViewById<RadioButton>(id)
        if (editTextName.text != null && editTextLastName.text != null && radioButton != null && spinnerOptions.selectedItem != null && date != null) {
            Log.d("Nombres", editTextName.text.toString())
            Log.d("Apellidos", editTextLastName.text.toString())
            Log.d("Sexo", radioButton.text.toString())
            Log.d("Fecha nacimiento", textViewDateOfBirth.text.toString())
            Log.d("Grado escolaridad", spinnerOptions.selectedItem.toString())
        } else {
            Log.d("ERROR", "Faltan datos por ingresar")
        }

    }
}
