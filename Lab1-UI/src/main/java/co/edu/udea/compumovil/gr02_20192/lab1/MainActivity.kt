package co.edu.udea.compumovil.gr02_20192.lab1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun OpenPersonalInfo(view: View) {
        val intent = Intent(this, PersonalInfo::class.java)
        startActivity(intent)
    }

    fun OpenContactInfo(view: View) {
        val intent = Intent(this, ContactInfo::class.java)
        startActivity(intent)
    }

}
